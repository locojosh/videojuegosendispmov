﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class P3_UIControl : MonoBehaviour
{
    public Toggle tglSound, tglMusic, tglVibration, tglNotifications;
    AudioSource audioSource;
    private void Start() 
    {
        audioSource = GetComponent<AudioSource>();

        //Load PlayerPrefs
        P3_UISaveSettings.LoadPlayerPrefs();

        //Load UI
        LoadSettings();
    }
    //SAVE/LOAD
    private void LoadSettings()
    {
        tglSound.isOn = P3_UISaveSettings.bSound;
        tglMusic.isOn = P3_UISaveSettings.bMusic;
        tglVibration.isOn = P3_UISaveSettings.bVibration;
        tglNotifications.isOn = P3_UISaveSettings.bNotifications;
    }
    public void Btn_OnClick_SaveSettings()
    {
        P3_UISaveSettings.SavePlayerPrefs();
    }
    //TOGGLES
    public void OnToggle_Sound()
    {
        PlayToggleSound();
        P3_UISaveSettings.bSound = tglSound.isOn;
    }
    public void OnToggle_Music()
    {
        PlayToggleSound();
        P3_UISaveSettings.bMusic = tglMusic.isOn;
    }
    public void OnToggle_Vibration()
    {
        PlayToggleSound();
        P3_UISaveSettings.bVibration = tglVibration.isOn;
    }
    public void OnToggle_Notifications()
    {
        PlayToggleSound();
        P3_UISaveSettings.bNotifications = tglNotifications.isOn;
    }
    private void PlayToggleSound()
    {
        audioSource.Play();
    }
}
