using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopBubble : MonoBehaviour
{
    private void Update() 
    {
        //Bubble
        if(Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Began)
        {
            Vector3 pos = Camera.main.ScreenToWorldPoint(Input.touches[0].position);
            RaycastHit2D hit = Physics2D.Raycast(pos, Vector2.zero);
            if(hit)
            {
                Debug.Log("Hit!");
                if(hit.collider.CompareTag("Bubble"))
                {
                    Color newColor = new Color(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), 1.0f);
                    hit.collider.GetComponent<SpriteRenderer>().color = newColor;
                }
            }
        }

        //Collectable
        if(Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Began)
        {
            Vector3 pos = Camera.main.ScreenToWorldPoint(Input.touches[0].position);
            Vector3 pos2 = new Vector3(pos.x, pos.y, 0);
            //Debug.Log(pos2);
            Collider2D col = Physics2D.OverlapPoint(pos2);
     
            if (col != null)
            {
                if(col.CompareTag("Collectable"))
                { Debug.Log("Hit Collectable!");
                    //Color newColor = new Color(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), 1.0f);
                    //hit.collider.GetComponent<SpriteRenderer>().color = newColor;
                    col.GetComponent<CollectableControl>().Collect();
                }     
            }

        }

        //OnPc();
    }
    private void OnPc()
    {
        //Bubble
        if(Input.GetMouseButtonDown(0))
        {
            Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(pos, Vector2.zero);
            if(hit)
            {
                
                if(hit.collider.CompareTag("Bubble"))
                { Debug.Log("Hit Bubble!");
                    Color newColor = new Color(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), 1.0f);
                    hit.collider.GetComponent<SpriteRenderer>().color = newColor;
                }
            }
        }
        //Collectable
        if(Input.GetMouseButtonDown(0))
        {
            Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector3 pos2 = new Vector3(pos.x, pos.y, 0);
            //Debug.Log(pos2);
            Collider2D col = Physics2D.OverlapPoint(pos2);
     
            if (col != null)
            {
                if(col.CompareTag("Collectable"))
                { Debug.Log("Hit Collectable!");
                    //Color newColor = new Color(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), 1.0f);
                    //hit.collider.GetComponent<SpriteRenderer>().color = newColor;
                    col.GetComponent<CollectableControl>().Collect();
                }     
            }

        }
    }
}
