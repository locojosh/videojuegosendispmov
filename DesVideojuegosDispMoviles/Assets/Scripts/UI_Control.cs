﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UI_Control : MonoBehaviour
{
    public GameObject[] panels;

    public void ShowPanel(string name)
    {
        foreach(var p in panels)
        {
            if(p.name == name)
                p.SetActive(!p.activeSelf);
        }
    }
    public void GoToScene(string name)
    {
        SceneManager.LoadScene(name);
    }
}
