﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(Rigidbody2D))]
public class KnightMove : MonoBehaviour
{
    public float velocity;
    Rigidbody2D rb;
    Animator animator;
    public FixedJoystick joystick;

    private void Awake() 
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }
    private void Update() 
    {
        //MoveJoystick();
        //Attack();
    }
    private void Move(float direction)
    {
        Vector2 newVelocity = new Vector2(direction*velocity*Time.deltaTime, 0);
        rb.velocity = newVelocity;
        AudioManagerGO.Instance.PlayFootstep();
    }    
    private void Attack()
    {
        animator.SetTrigger("attack");
        AudioManagerGO.Instance.Play("attack");
    }
    private void MoveJoystick()
    {
        Move(joystick.Horizontal);
        if(joystick.Horizontal == 0)
        {
            animator.SetBool("run", false);
        }
        else
        {
            animator.SetBool("run", true);
        }
    }
    
    public void OnClick_Attack()
    {
        Attack();
    }
    public void OnTap_MoveRight()
    {
        Move(1);
        animator.SetBool("run", true);
    }
    public void OnTap_MoveLeft()
    {
        Move(-1);
        animator.SetBool("run", true);
    }
    public void OnStopTap_StopMoving()
    {
        Move(0);
        animator.SetBool("run", false);
    }
    //input system
    public void Move(InputAction.CallbackContext context)
    {
        Move(context.ReadValue<float>());
    }
    public void OnAttack(InputAction.CallbackContext context)
    {
        Attack();
    }
}
