﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectableControl : MonoBehaviour
{
    public string collectableType; //coins or gems
    Collider2D col;

    private void Start() 
    {
        col = GetComponent<Collider2D>();
        col.isTrigger = true;
    }
    public void Collect()
    {
        MyGameManager.instance.AddCollectable(collectableType);
        AudioManagerGO.Instance.Play(collectableType);
        Destroy(gameObject);
    }
}
