﻿using UnityEngine;
using System.IO; //to work with files
using System.Runtime.Serialization.Formatters.Binary; //

public static class GameSaveLoad
{
    public static void Save(int nMatch, int nLevel, string what, Player player)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/game" + nMatch +nLevel+what +".fun";
        FileStream stream = new FileStream(path, FileMode.Create);

        GameData data = new GameData(player);

        formatter.Serialize(stream, data);
        stream.Close();
    }
    public static void Save(int nMatch, string what, MyGameManager gameManager)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/game" + nMatch +what +".fun";
        FileStream stream = new FileStream(path, FileMode.Create);

        GameData data = new GameData(gameManager);

        formatter.Serialize(stream, data);
        stream.Close();
    }
    public static GameData Load(int nMatch, int nLevel, string what)
    {
        string path = Application.persistentDataPath + "/game" + nMatch +nLevel+what+ ".fun";
        if(File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            GameData data = formatter.Deserialize(stream) as GameData;
            stream.Close();

            return data;
        }
        else
        {
            Debug.LogError("Save file not found in " + path);
            return null;
        }
    }    
    public static GameData Load(int nMatch, string what)
    {
        string path = Application.persistentDataPath + "/game" + nMatch +what+ ".fun";
        if(File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            GameData data = formatter.Deserialize(stream) as GameData;
            stream.Close();

            return data;
        }
        else
        {
            Debug.LogError("Save file not found in " + path);
            return null;
        }
    }    
}
